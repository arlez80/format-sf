# format-sf

Soundfont Reader for Haxe Programming Language.

# Usage

```
#!haxe

// for read
var fileName = "test.sf"
var sfReader = new format.sf.Reader( sys.io.File.read( fileName ) );
var sf = sfReader.read( );

```

# License

MIT License