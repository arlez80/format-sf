package format.sf;

import haxe.io.Bytes;

typedef SoundFont = {
	info:SoundFontInfo,
	sdta:SoundFontSDTA,
	pdta:SoundFontPreset
}

typedef SoundFontInfo = {
	ifil:SoundFontVersionTag,
	isng:String,
	inam:String,

	irom:String,
	iver:SoundFontVersionTag,
	icrd:String,
	ieng:String,
	iprd:String,
	icop:String,
	icmt:String,
	isft:String,
}

typedef SoundFontSDTA = {
	smpl:Bytes,
	sm24:Bytes,
}

typedef SoundFontPreset = {
	phdr:Array<SoundFontPresetHeader>,
	pbag:Array<SoundFontBag>,
	pmod:Array<SoundFontModList>,
	pgen:Array<SoundFontGenList>,
	inst:Array<SoundFontInst>,
	ibag:Array<SoundFontBag>,
	imod:Array<SoundFontModList>,
	igen:Array<SoundFontGenList>,
	shdr:Array<SoundFontSample>,
}

@:enum
abstract SoundFontSampleLink(Int)
{
	var monoSample = 1;
	var rightSample = 2;
	var leftSample = 4;
	var linkedSample = 8;
	var RomMonoSample = 0x8001;
	var RomRightSample = 0x8002;
	var RomLeftSample = 0x8004;
	var RomLinkedSample = 0x8008;
}

typedef SoundFontVersionTag = {
	major:Int,
	minor:Int,
}

typedef SoundFontPresetHeader = {
	name:String,
	preset:Int,			// MIDI Instruments Number
	bank:Int,			// MIDI Bank
	presetBagIndex:Int,
	library:Int,
	genre:Int,
	morphology:Int,
}

typedef SoundFontBag = {
	genNdx:Int,
	modNdx:Int,
}

typedef SoundFontInst = {
	name:String,
	instBagNdx:Int,
}

typedef SoundFontModList = {
	srcOper:SoundFontModulator,
	destOper:SoundFontGenerator,
	amount:Int,
	amtSrcOper:SoundFontModulator,
	transOper:SoundFontTransform,
}

@:enum
abstract SoundFontTransform(Int)
{
	var linear = 0;
	var absoluteValue = 2;
}

typedef SoundFontModulator = {
	type:SoundFontModulatorType,
	direction:SoundFontModulatorDirection,
	polarity:SoundFontModulatorPolarity,
	controller:SoundFontModulatorController,
	controllerPallete:SoundFontModulatorControllerPallete,
}

@:enum
abstract SoundFontModulatorController(Int)
{
	var noController = 0;
	var noteOnVelocity = 2;
	var noteOnKeyNumber = 3;
	var polyPressure = 10;
	var channelPressure = 13;
	var pitchWheel = 14;
	var pitchWheelSensitivity = 16;
	var link = 127;
}

@:enum
abstract SoundFontModulatorControllerPallete(Int)
{
	var generalController = 0;
	var midiController = 1;
}

@:enum
abstract SoundFontModulatorDirection(Int)
{
	var increase = 0;
	var decrease = 1;
}

@:enum
abstract SoundFontModulatorPolarity(Int)
{
	var unipolar = 0;
	var bipolar = 1;
}

@:enum
abstract SoundFontModulatorType(Int)
{
	var linear = 0;
	var concave = 1;
	var convex = 2;
	var cswitch = 3;
}

typedef SoundFontGenList = {
	genOper:SoundFontGenerator,
	amount:SoundFontGenAmountType,
}

typedef SoundFontGenAmountType = Int;

typedef SoundFontSample = {
	name:String,
	start:Int,
	end:Int,
	startLoop:Int,
	endLoop:Int,
	sampleRate:Int,
	originalKey:Int,
	pitchCorrection:Int,
	sampleLink:Int,
	sampleType:SoundFontSampleLink,
}

@:enum
abstract SoundFontGenerator(Int)
{
	var startAddrsOffset = 0;
	var endAddrsOffset = 1;
	var startloopAddrsOffset = 2;
	var endloopAddrsOffset = 3;
	var startAddrsCoarseOffset = 4;
	var modLfoToPitch = 5;
	var vibLfoToPitch = 6;
	var modEnvToPitch = 7;
	var initialFilterFc = 8;
	var initialFilterQ = 9;
	var modLfoToFilterFc = 10;
	var modEnvToFilterFc = 11;
	var endAddrsCoarseOffset = 12;
	var modLfoToVolume = 13;
	var unused1 = 14;
	var chorusEffectsSend = 15;
	var reverbEffectsSend = 16;
	var pan = 17;
	var unused2 = 18;
	var unused3 = 19;
	var unused4 = 20;
	var delayModLFO = 21;
	var freqModLFO = 22;
	var delayVibLFO = 23;
	var freqVibLFO = 24;
	var delayModEnv = 25;
	var attackModEnv = 26;
	var holdModEnv = 27;
	var decayModEnv = 28;
	var sustainModEnv = 29;
	var releaseModEnv = 30;
	var keynumToModEnvHold = 31;
	var keynumToModEnvDecay = 32;
	var delayVolEnv = 33;
	var attackVolEnv = 34;
	var holdVolEnv = 35;
	var decayVolEnv = 36;
	var sustainVolEnv = 37;
	var releaseVolEnv = 38;
	var keynumToVolEnvHold = 39;
	var keynumToVolEnvDecay = 40;
	var instrument = 41;
	var reserved1 = 42;
	var keyRange = 43;
	var velRange = 44;
	var startloopAddrsCoarseOffset = 45;
	var keynum = 46;
	var velocity = 47;
	var initialAttenuation = 48;
	var reserved2 = 49;
	var endloopAddrsCoarseOffset = 50;
	var coarseTune = 51;
	var fineTune = 52;
	var sampleID = 53;
	var sampleModes = 54;
	var reserved3 = 55;
	var scaleTuning = 56;
	var exclusiveClass = 57;
	var overridingRootKey = 58;
	var unused5 = 59;
	var endOper = 60;
}
