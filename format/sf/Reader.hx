package format.sf;

import format.sf.Data;
import haxe.io.BytesInput;
import haxe.io.Input;

/**
 * SoundFont Reader
 * @author あるる（きのもと 結衣）
 */
class Reader 
{
	/// データソース
	private var data:Input;

	/**
	 * コンストラクタ
	 * @param	data
	 */
	public function new( data:Input ) 
	{
		this.data = data;
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):SoundFont
	{
		this.checkChunk( this.data, "RIFF" );
		this.checkHeader( this.data, "sfbk" );

		var info = this.readInfo( );
		var sdta = this.readSDTA( );
		var pdta = this.readPDTA( );

		return {
			info: info,
			sdta: sdta,
			pdta: pdta
		};
	}

	/**
	 * ヘッダを確認
	 * @param	hdr
	 */
	private function checkHeader( bi:Input, hdr:String ):Void
	{
		if ( hdr != bi.readString( 4 ) ) {
			throw "Doesn't have " + hdr + " header";
		}
	}

	/**
	 * チャンクデータを確認する
	 * @param	hdr
	 * @return
	 */
	private function checkChunk( bi:Input, hdr:String ):Int
	{
		this.checkHeader( bi, hdr );
		var size = bi.readInt32( );
		return size;
	}

	/**
	 * チャンクを読む
	 * @param	bi
	 * @return	chunk data
	 */
	private function readChunk( bi:Input ):{ hdr:String, size:Int, bi:BytesInput }
	{
		var hdr = bi.readString( 4 );
		var size = bi.readInt32( );
		var bi = new BytesInput( bi.read( size ) );

		return {
			hdr: hdr,
			size: size,
			bi: bi,
		};
	}

	/**
	 * 0終端の文字列を読み込む
	 * @param	bi
	 * @return
	 */
	private function readZeroTerminatedString( bi:BytesInput, max:Int ):String
	{
		var sb = new StringBuf( );
		var i:Int = 0;
		var avoid = false;

		while ( i < max ) {
			var c = bi.readByte( );
			if ( c == 0 ) avoid = true;
			if( ! avoid ) sb.addChar( c );
			i ++;
		}

		return sb.toString( );
	}

	/**
	 * Infoを読み込む
	 * @return
	 */
	private function readInfo( ):SoundFontInfo
	{
		var infoChunk = this.readChunk( this.data );
		var bi = infoChunk.bi;
		this.checkHeader( bi, "INFO" );

		var info = {
			ifil:null,
			isng:null,
			inam:null,

			irom:null,
			iver:null,
			icrd:null,
			ieng:null,
			iprd:null,
			icop:null,
			icmt:null,
			isft:null,
		};

		while ( bi.position < bi.length ) {
			var chunk = this.readChunk( bi );
			switch( chunk.hdr.toLowerCase( ) ) {
				case "ifil": info.ifil = this.readVersionTag( chunk.bi );
				case "isng": info.isng = chunk.bi.readString( chunk.size );
				case "inam": info.inam = chunk.bi.readString( chunk.size );
				case "irom": info.irom = chunk.bi.readString( chunk.size );
				case "iver": info.iver = this.readVersionTag( chunk.bi );
				case "icrd": info.irom = chunk.bi.readString( chunk.size );
				case "ieng": info.irom = chunk.bi.readString( chunk.size );
				case "iprd": info.irom = chunk.bi.readString( chunk.size );
				case "icop": info.irom = chunk.bi.readString( chunk.size );
				case "icmt": info.irom = chunk.bi.readString( chunk.size );
				case "isft": info.irom = chunk.bi.readString( chunk.size );
				default:
					throw "Unknown header of INFO: " + chunk.hdr;
			}
		}

		return info;
	}

	/**
	 * バージョンタグを読み込む
	 * @param	bi
	 * @return
	 */
	private function readVersionTag( bi:BytesInput ):SoundFontVersionTag
	{
		var major = bi.readUInt16( );
		var minor = bi.readUInt16( );

		return {
			major: major,
			minor: minor,
		};
	}

	/**
	 * SDTAを読み込む
	 * @param	bi
	 * @return
	 */
	private function readSDTA( ):SoundFontSDTA
	{
		var sdtaChunk = this.readChunk( this.data );
		var bi = sdtaChunk.bi;
		this.checkHeader( bi, "sdta" );

		var smpl = this.readChunk( bi );
		if ( smpl.hdr != "smpl" ) throw "Doesnt exist smpl header.";
		var smplBytes = smpl.bi.readAll( );

		var sm24Bytes = if ( bi.position < bi.length ) {
			var c = this.readChunk( bi );
			if ( c.hdr != "sm24" ) throw "Doesnt exist sm24 header.";
			c.bi.readAll( );
		}else {
			null;
		}

		return {
			smpl: smplBytes,
			sm24: sm24Bytes,
		};
	}

	/**
	 * PDTA読み込み
	 * @return
	 */
	private function readPDTA( ):SoundFontPreset
	{
		var pdtaChunk = this.readChunk( this.data );
		var bi = pdtaChunk.bi;
		this.checkHeader( bi, "pdta" );

		var phdr = this.readPDTAPhdr( bi );
		var pbag = this.readPDTABag( bi );
		var pmod = this.readPDTAMod( bi );
		var pgen = this.readPDTAGen( bi );
		var inst = this.readPDTAInst( bi );
		var ibag = this.readPDTABag( bi );
		var imod = this.readPDTAMod( bi );
		var igen = this.readPDTAGen( bi );
		var shdr = this.readPDTASample( bi );

		return {
			phdr:phdr,
			pbag:pbag,
			pmod:pmod,
			pgen:pgen,
			inst:inst,
			ibag:ibag,
			imod:imod,
			igen:igen,
			shdr:shdr,
		};
	}

	/**
	 * phdr読み込み
	 * @return
	 */
	private function readPDTAPhdr( src:BytesInput ):Array<SoundFontPresetHeader>
	{
		var phdrChunk = this.readChunk( src );
		if( phdrChunk.hdr.substr( 1 ) != "phdr" ) "Doesnt exist phdr header.";
		var bi = phdrChunk.bi;
		var phdrs = new Array<SoundFontPresetHeader>( );

		while( bi.position < bi.length ) {
			var phdr = {
				name:"",
				preset:0,
				bank:0,
				presetBagIndex:0,
				library:0,
				genre:0,
				morphology:0,
			};

			phdr.name = this.readZeroTerminatedString( bi, 20 );
			phdr.preset = bi.readUInt16( );
			phdr.bank = bi.readUInt16( );
			phdr.presetBagIndex = bi.readUInt16( );
			phdr.library = bi.readInt32( );
			phdr.genre = bi.readInt32( );
			phdr.morphology = bi.readInt32( );

			phdrs.push( phdr );
		}

		return phdrs;
	}

	/**
	 * pbag読み込み
	 * @return
	 */
	private function readPDTABag( src:BytesInput ):Array<SoundFontBag>
	{
		var pbagChunk = this.readChunk( src );
		if( pbagChunk.hdr.substr( 1 ) != "bag" ) "Doesnt exist *bag header.";
		var bi = pbagChunk.bi;
		var pbags = new Array<SoundFontBag>( );

		while ( bi.position < bi.length ) {
			var pbag = {
				genNdx: 0,
				modNdx: 0,
			};

			pbag.genNdx = bi.readUInt16( );
			pbag.modNdx = bi.readUInt16( );
			pbags.push( pbag );
		}

		return pbags;
	}

	/**
	 * pmod読み込み
	 * @return
	 */
	private function readPDTAMod( src:BytesInput ):Array<SoundFontModList>
	{
		var pmodChunk = this.readChunk( src );
		if( pmodChunk.hdr.substr( 1 ) != "mod" ) "Doesnt exist *mod header.";
		var bi = pmodChunk.bi;
		var pmods = new Array<SoundFontModList>( );

		while ( bi.position < bi.length ) {
			var pmod:SoundFontModList = {
				srcOper: null,
				destOper: SoundFontGenerator.endOper,
				amount: 0,
				amtSrcOper: null,
				transOper: SoundFontTransform.linear,
			};

			pmod.srcOper = this.readModulator( bi.readUInt16( ) );
			pmod.destOper = cast bi.readUInt16( );
			pmod.amount = bi.readInt16( );
			pmod.amtSrcOper = this.readModulator( bi.readUInt16( ) );
			pmod.transOper = cast bi.readUInt16( );
			pmods.push( pmod );
		}

		return pmods;
	}

	/**
	 * pgen読み込み
	 * @return
	 */
	private function readPDTAGen( src:BytesInput ):Array<SoundFontGenList>
	{
		var pgenChunk = this.readChunk( src );
		if( pgenChunk.hdr.substr( 1 ) != "gen" ) "Doesnt exist *gen header.";
		var bi = pgenChunk.bi;
		var pgens = new Array<SoundFontGenList>( );

		while ( bi.position < bi.length ) {
			var pgen:SoundFontGenList = {
				genOper: cast 0,
				amount: 0,
			};

			pgen.genOper = cast bi.readInt16( );
			pgen.amount = bi.readUInt16( );
			pgens.push( pgen );
		}

		return pgens;
	}

	/**
	 * モジュレータ設定読み込み
	 */
	private function readModulator( u:Int ):SoundFontModulator
	{
		return {
			type: cast( ( u >> 10 ) & 0x3f ),
			direction: cast( ( u >> 8 ) & 0x01 ),
			polarity: cast( ( u >> 9 ) & 0x01 ),
			controller: cast( u & 0x7f ),
			controllerPallete: cast( ( u >> 7 ) & 0x01 ),
		};
	}

	/**
	 * inst読み込み
	 * @return
	 */
	private function readPDTAInst( src:BytesInput ):Array<SoundFontInst>
	{
		var instChunk = this.readChunk( src );
		if( instChunk.hdr != "inst" ) "Doesnt exist inst header.";
		var bi = instChunk.bi;
		var insts = new Array<SoundFontInst>( );

		while ( bi.position < bi.length ) {
			var inst = {
				name: "",
				instBagNdx: 0
			};

			inst.name = this.readZeroTerminatedString( bi, 20 );
			inst.instBagNdx = bi.readUInt16( );
			insts.push( inst );
		}

		return insts;
	}

	/**
	 * shdr読み込み
	 * @return
	 */
	private function readPDTASample( src:BytesInput ):Array<SoundFontSample>
	{
		var shdrChunk = this.readChunk( src );
		if( shdrChunk.hdr != "shdr" ) "Doesnt exist shdr header.";
		var bi = shdrChunk.bi;
		var shdrs = new Array<SoundFontSample>( );

		while ( bi.position < bi.length ) {
			var shdr:SoundFontSample = {
				name:"",
				start:0,
				end:0,
				startLoop:0,
				endLoop:0,
				sampleRate:0,
				originalKey:0,
				pitchCorrection:0,
				sampleLink:0,
				sampleType:SoundFontSampleLink.monoSample,
			};

			shdr.name = this.readZeroTerminatedString( bi, 20 );
			shdr.start = bi.readInt32( );
			shdr.end = bi.readInt32( );
			shdr.startLoop = bi.readInt32( );
			shdr.endLoop = bi.readInt32( );
			shdr.sampleRate = bi.readInt32( );
			shdr.originalKey = bi.readByte( );
			shdr.pitchCorrection = bi.readInt8( );
			shdr.sampleLink = bi.readUInt16( );
			shdr.sampleType = cast bi.readUInt16( );
			shdrs.push( shdr );
		}

		return shdrs;
	}
}
