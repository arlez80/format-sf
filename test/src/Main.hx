package;

import haxe.io.BytesInput;
import neko.Lib;
import sys.io.File;
import format.sf.Data;

/**
 * ...
 * @author あるる（きのもと 結衣）
 */
class Main 
{
	
	static function main() 
	{
		var sfReader = new format.sf.Reader( File.read( "Slap Bass.SF2" ) );
		var sf = sfReader.read( );
		displaySoundFont( sf );

		var sfReader = new format.sf.Reader( File.read( "Aspirin-Stereo.sf2" ) );
		var sf = sfReader.read( );
		displaySoundFont( sf );
	}

	static private function displaySoundFont( sf:SoundFont ):Void
	{
		trace( "---------------------------------" );
		trace( "** INFO **" );
		trace( sf.info );

		trace( "** SDTA **" );
		trace( "smpl size:", sf.sdta.smpl.length );
		trace( "sm24 size:", sf.sdta.sm24 != null ? sf.sdta.sm24.length : "null" );

		trace( "** PDTA **" );
		trace( "phdr count:", sf.pdta.phdr.length );
		trace( "pbag count:", sf.pdta.pbag.length );
		trace( "pmod count:", sf.pdta.pmod.length );
		trace( "pgen count:", sf.pdta.pgen.length );
		trace( "inst count:", sf.pdta.inst.length );
		trace( "ibag count:", sf.pdta.ibag.length );
		trace( "imod count:", sf.pdta.imod.length );
		trace( "igen count:", sf.pdta.igen.length );
		trace( "shdr count:", sf.pdta.shdr.length );
	}
}
